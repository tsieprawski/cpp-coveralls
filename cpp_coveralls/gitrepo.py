from __future__ import absolute_import

import os
import subprocess


def gitrepo(cwd):
    """Return hash of Git data that can be used to display more information to
    users.

    Example:
        "git": {
            "head": {
                "id": "5e837ce92220be64821128a70f6093f836dd2c05",
                "author_name": "Wil Gieseler",
                "author_email": "wil@example.com",
                "committer_name": "Wil Gieseler",
                "committer_email": "wil@example.com",
                "message": "depend on simplecov >= 0.7"
            },
            "branch": "master",
            "remotes": [{
                "name": "origin",
                "url": "https://github.com/lemurheavy/coveralls-ruby.git"
            }]
        }

    From https://github.com/coagulant/coveralls-python (with MIT license).

    """
    repo = Repository(cwd)
    if not repo.valid():
        return {}

    return {
        'head': {
            'id': repo.hglog('{node}'),
            'author_name': repo.hglog('{author}'),
            'author_email': '',
            'committer_name': repo.hglog('{author}'),
            'committer_email': '',
            'message': repo.hglog('{desc}')
        },
        'branch': os.environ.get('TRAVIS_BRANCH',
                  os.environ.get('APPVEYOR_REPO_BRANCH',
                                 repo.hg('branch')[1])),
        'remotes': []
    }


class Repository(object):

    def __init__(self, cwd):
        self.cwd = cwd

    def valid(self):
        """
        :return: true if it is a valid git repository.
        """
        return self.hg("log", "-l", "1")[0] == 0

    def hglog(self, fmt):
        return self.hg("log", "-l", "1", "--template", fmt)[1]

    def hg(self, *arguments):
        """
        Return (exit code, output) from hg.
        """
        process = subprocess.Popen(['hg'] + list(arguments),
                                   stdout=subprocess.PIPE,
                                   cwd=self.cwd)
        stdoutdata = process.communicate()
        out = stdoutdata[0].decode('UTF-8')
        code = process.returncode
        return code, out
